﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Newtonsoft.Json;
using Windows.UI.Popups;
using System.Threading.Tasks;
using Windows.Storage;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Altarix
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {

        string login;

        public MainPage()
        {
            this.InitializeComponent();

            this.NavigationCacheMode = NavigationCacheMode.Required;
            
        }

        void SetLogin()
        {
            ApplicationDataContainer ls = ApplicationData.Current.LocalSettings;
            if (ls.Values.Keys.Contains("login"))
            {
                login = ls.Values["login"].ToString();
            }
            else
            {
                login = "";
            }
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            // TODO: Prepare page for display here.

            // TODO: If your application contains multiple pages, ensure that you are
            // handling the hardware Back button by registering for the
            // Windows.Phone.UI.Input.HardwareButtons.BackPressed event.
            // If you are using the NavigationHelper provided by some templates,
            // this event is handled for you.
        }

        //private RootObject rootObject;

        private async Task<RootObject> GetJsonObject()
        {
            if(login == "")
            {
               await new MessageDialog("Please, enter your login in settings", "Enter login").ShowAsync();
               return null;
            }
            string url = @"http://pinterestapi.co.uk/" + login + @"/pins";
            HttpClient httpClient = new HttpClient();

            httpClient.DefaultRequestHeaders.Accept.TryParseAdd("application/json");

            RootObject rootObject = new RootObject();
            try
            {
                string jsonResponse = await httpClient.GetStringAsync(new Uri(url));
                rootObject = JsonConvert.DeserializeObject<RootObject>(jsonResponse);
            }
            catch (HttpRequestException e)
            {
                listView.Items.Clear();
                string errorString = "Unable to get json (" + e.HResult.ToString() + ") " + e.Message.ToString();
                await new MessageDialog(errorString, "Error").ShowAsync();
                rootObject = null;
            }
            return rootObject;
        }

        private async void LoadFeeds()
        {

            RootObject rootObject = await GetJsonObject();

            if(rootObject == null)
            {
                return;
            }

            for(int i=0; i < rootObject.body.Count; i++)
            {
                object desc = rootObject.body[i].desc;
                if (desc is bool)
                {
                    if (!(bool)desc)
                    {
                        desc = (string)"No description";
                    }
                }
                rootObject.body[i].desc = desc;
            }

            listView.Items.Clear();
            foreach (Body b in rootObject.body)
            {
                listView.Items.Add((Altarix.Body)b);
            }

           
        }

        private void listView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                Frame.Navigate(typeof(DetailsPost), e.AddedItems[0]);
            }
            catch
            {

            }
        }

        private void listView_Tapped(object sender, TappedRoutedEventArgs e)
        {
            
        }

        private void AppBarButton_Click(object sender, RoutedEventArgs e)
        {
            LoadFeeds();
        }

        private void AppBarButton_Click_1(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(SettingsPage));
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            SetLogin();

            LoadFeeds();
        }
    }
}
